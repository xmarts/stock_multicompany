# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.addons import decimal_precision as dp


class StockMulticompanyProductProduct(models.Model):
    _inherit = 'product.product'

    sudo_qty_available = fields.Float(
        'Quantity On Hand', compute='_compute_quantities_stock_multicompany', search='_search_qty_available',
        digits=dp.get_precision('Product Unit of Measure'),
        help="Current quantity of products.\n"
             "In a context with a single Stock Location, this includes "
             "goods stored at this Location, or any of its children.\n"
             "In a context with a single Warehouse, this includes "
             "goods stored in the Stock Location of this Warehouse, or any "
             "of its children.\n"
             "stored in the Stock Location of the Warehouse of this Shop, "
             "or any of its children.\n"
             "Otherwise, this includes goods stored in any Stock Location "
             "with 'internal' type.")
    sudo_virtual_available = fields.Float(
        'Forecast Quantity', compute='_compute_quantities_stock_multicompany', search='_search_virtual_available',
        digits=dp.get_precision('Product Unit of Measure'),
        help="Forecast quantity (computed as Quantity On Hand "
             "- Outgoing + Incoming)\n"
             "In a context with a single Stock Location, this includes "
             "goods stored in this location, or any of its children.\n"
             "In a context with a single Warehouse, this includes "
             "goods stored in the Stock Location of this Warehouse, or any "
             "of its children.\n"
             "Otherwise, this includes goods stored in any Stock Location "
             "with 'internal' type.")

    @api.depends('stock_quant_ids', 'stock_move_ids')
    def _compute_quantities_stock_multicompany(self):
        sSelf = self.sudo()
        res = sSelf._compute_quantities_dict(sSelf._context.get('lot_id'), sSelf._context.get('owner_id'),
                                             sSelf._context.get('package_id'), sSelf._context.get('from_date'),
                                             sSelf._context.get('to_date'))
        for product in self:
            product.sudo_qty_available = res[product.id]['qty_available']
            product.sudo_virtual_available = res[product.id]['virtual_available']
