# -*- coding: utf-8 -*-
from odoo import models, fields, api, exceptions
from odoo.tools.float_utils import float_compare


class StockMuticompanyStockPicking(models.Model):
    _inherit = 'stock.picking'

    state = fields.Selection([
        ('draft', 'Draft'), ('cancel', 'Cancelled'),
        ('waiting', 'Waiting Another Operation'),
        ('confirmed', 'Waiting Availability'),
        ('partially_available', 'Partially Available'),
        ('assigned', 'Available'), ('done', 'Done'),('consignacion', 'consignacion')], string='Status', compute='_compute_state',
        copy=False, index=True, readonly=True, store=True, track_visibility='onchange',
        help=" * Draft: not confirmed yet and will not be scheduled until confirmed\n"
             " * Waiting Another Operation: waiting for another move to proceed before it becomes automatically available (e.g. in Make-To-Order flows)\n"
             " * Waiting Availability: still waiting for the availability of products\n"
             " * Partially Available: some products are available and reserved\n"
             " * Ready to Transfer: products reserved, simply waiting for confirmation.\n"
             " * Transferred: has been processed, can't be modified or cancelled anymore\n"
             " * Cancelled: has been cancelled, can't be confirmed anymore")



    def do_prepare_partial_consignacion(self):
        # TDE CLEANME: oh dear ...
        sSelf = self.sudo()
        PackOperation = self.env['stock.pack.operation']

        # get list of existing operations and delete them
        existing_packages = PackOperation.search([('picking_id', 'in', self.ids)])  # TDE FIXME: o2m / m2o ?
        if existing_packages:
            existing_packages.unlink()
        for picking in sSelf:
            forced_qties = {}  # Quantity remaining after calculating reserved quants

            picking_quants = sSelf.env['stock.quant']
            # Calculate packages, reserved quants, qtys of this picking's moves
            for move in picking.move_lines:
                if move.state not in ('assigned', 'confirmed', 'waiting','consignacion'):
                    continue
                move_quants = move.reserved_quant_ids
                picking_quants += move_quants
                forced_qty = 0.0
                if move.state == 'assigned':
                    qty = move.product_uom._compute_quantity(move.product_uom_qty, move.product_id.uom_id, round=False)
                    if move.product_id.qty_available - qty < 0:

                        forced_qty = move.product_id.qty_available

                    else:

                        forced_qty = qty - sum([x.qty for x in move_quants])

                # if we used force_assign() on the move, or if the move is incoming, forced_qty > 0
                if float_compare(forced_qty, 0, precision_rounding=move.product_id.uom_id.rounding) > 0:
                    if forced_qties.get(move.product_id):
                        forced_qties[move.product_id] += forced_qty
                    else:
                        forced_qties[move.product_id] = forced_qty
            for vals in picking._prepare_pack_ops(picking_quants, forced_qties):
                vals['fresh_record'] = False
                PackOperation.create(vals)
        # recompute the remaining quantities all at once
        self.do_recompute_remaining_quantities()
        self.write({'recompute_pack_op': False})




    @api.multi
    def consignar(self):
        """ Changes state of picking to available if moves are confirmed or waiting.
        @return: True
        """
        sSelf = self.sudo()
        for line in sSelf.move_lines:
            if line.product_id.qty_available > 0:
                if line.state in  ['waiting','confirmed']:
                    line.state = 'consignacion'
                    #real_line = self.env['stock.move'].search([('id','=',line.id)])
                    #real_line.write({'state':'consignacion'})
        self.mapped('move_lines').filtered(lambda move: move.state in ['consignacion']).force_assign_consignacion()
        return True



    # TDE DECORATOR: internal
    @api.multi
    def check_recompute_pack_op_consignacion(self):
        pickings = self.mapped('picking_id').filtered(lambda picking: picking.state not in ('waiting', 'confirmed'))  # In case of 'all at once' delivery method it should not prepare pack operations sale en blanco cuando no tiene
        # Check if someone was treating the picking already
        pickings_partial = pickings.filtered(lambda picking: not any(operation.qty_done for operation in picking.pack_operation_ids)) #si tiene disponible se pone igual que el otro
        pickings_partial.do_prepare_partial_consignacion()
        (pickings - pickings_partial).write({'recompute_pack_op': True})

