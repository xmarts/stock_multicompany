# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, float_compare


class StockMultiCompanySaleOrderLine(models.Model):
    _name = 'sale.order.line'
    _inherit = 'sale.order.line'

    @api.multi
    def _action_procurement_create(self):
        res = super(StockMultiCompanySaleOrderLine, self)._action_procurement_create()
        orders = list(set(x.order_id for x in self))
        for order in orders:
            reassign = order.picking_ids.filtered(
                lambda x: x.state == 'confirmed' or ((x.state in ['partially_available', 'waiting']) and not x.printed))
            if reassign:
                reassign.do_unreserve()
                reassign.action_assign()
                reassign.consignar()

        return res




    @api.onchange('product_uom_qty', 'product_uom', 'route_id')
    def _onchange_product_id_check_availability(self):

        #res = super(StockMultiCompanySaleOrderLine,self)._onchange_product_id_check_availability()

        if not self.product_id or not self.product_uom_qty or not self.product_uom:
            self.product_packaging = False
            return {}#res
        if self.product_id.type == 'product':
            self.sudo()
            precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
            product_qty = self.product_uom._compute_quantity(self.product_uom_qty, self.product_id.uom_id)

            sSelf = self.sudo()
            sProduct = sSelf.env['product.template'].search([('id', '=', self.product_id.id)])

            if float_compare(sProduct.virtual_available, product_qty, precision_digits=precision) == -1:
                is_available = self._check_routing()
                if not is_available:
                    warning_mess = {
                        'title': _('Not enough inventory!'),
                        'message' : _('You plan to sell %s %s but you only have %s %s available!\nThe stock on hand is %s %s.') % \
                            (self.product_uom_qty, self.product_uom.name, sProduct.virtual_available, sProduct.uom_id.name, sProduct.qty_available, sProduct.uom_id.name)
                    }
                    #res.update({'warning': warning_mess})
                    return {'warning': warning_mess}
        return {}

        # 'You plan to sell %s %s but you only have %s %s available!\nThe stock on hand is %s %s.') % \